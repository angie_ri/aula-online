
@extends('layouts.public')
@section('title','| Ingresar')
@section('content')
<!-- Page Content -->
<div class="bg-image ">
    <div class="hero-static "style="background-image: url('{{asset('/storage/mate1.png')}}');" >
        <div class="content ">
            <div class="row justify-content-center ">
                <div class="col-md-8 col-lg-6 col-xl-4 ">
                    <!-- Sign In Block -->
                    <div class="block block-themed  mb-0 ">
                        <div class="block-header">
                            <h3 class="block-title">Ingresar</h3>
                            <div class="block-options">
                                <a class="btn-block-option" href="{{url('/')}}/registrar" data-toggle="tooltip" data-placement="left" title="Nueva cuenta">Registrarse
                                    <i class="fa fa-user-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="block-content" >
                            <div class="p-sm-3 px-lg-4 py-lg-5">
                                <h1 class="mb-2">ColeEnCasa</h1>
                                <p>Bienvenido, por favor ingrese.</p>

                                <form class="js-validation-signin" method="post" action="{{ URL::to('/') }}/ingresar" >
                                    @csrf
                                    <div class="py-3">
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-alt form-control-lg" id="login-username" name="email" placeholder="Correo electrónico">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-alt form-control-lg" id="login-password" name="password" placeholder="Contraseña">
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6 col-xl-5">
                                            <button  class="btn btn-block btn-primary" type="submit">
                                                <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Ingresar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- END Sign In Form -->
                            </div>
                        </div>
                    </div>
                    <!-- END Sign In Block -->
                </div>
            </div>
        </div>
        <!-- Get Started -->
        <div class="">
            <div class="content content-full" >
                <div class="my-5 text-center">
                    <h2 class="h3 mb-4 invisible text-light" data-toggle="appear">#aprendeDesdeCasa</h2>
                    <a class="btn btn-rounded btn-primary px-4 py-2 " data-toggle="appear" data-class="animated bounceIn" href="{{url('/')}}/">Regresar a Inicio</a>
                </div>
            </div>
        </div>
        <!-- END Get Started -->

    </div>
</div>

@endsection

