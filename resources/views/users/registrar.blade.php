
@extends('layouts.public')
@section('title','| Registrar')

@section('content')
<!-- Page Content -->
    <div class="hero-static " style="background-image: url('{{asset('storage/mate1.png')}}');"   >
        <div class="content " >
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-4">
                    <!-- Sign Up Block -->
                    <div class="alert alert-primary" role="alert" style="display: none" id="alert-reg">

                    </div>

                    <div class="block block-themed mb-0">

                        <div class="block-header">
                            <h3 class="block-title">Registrarse</h3>
                            <div class="block-options">
                                <a class="btn-block-option" href="{{url('/')}}/ingresar" data-toggle="tooltip" data-placement="left" title="Ir a Ingresar">Ingresar
                                    <i class="fa fa-user"></i>
                                </a>
                            </div>
                        </div>
                        <div class="block-content" >
                            <div class="p-sm-3 px-lg-4 py-lg-5">
                                <h1 class="mb-2">ColeEnCasa</h1>
                                <p>Por favor ingrese sus datos para registrarse.</p>

                                <form class="js-validation-signup" id="form-registrar" method="post" >
                                    <div class="py-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-lg form-control-alt" id="name" name="name" placeholder="Nombres y Apellidos">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-lg form-control-alt" id="email" name="email"  placeholder="Correr Eléctronico">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-lg form-control-alt" id="password" name="password" placeholder="Contraseña">
                                        </div>
                                        <div class="form-group">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6 col-xl-5">
                                            <button type="button" class="btn btn-block btn-primary" onclick="registrar()">
                                                Registrarme
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- END Sign Up Form -->

                            </div>
                        </div>
                    </div>
                    <!-- END Sign Up Block -->
                    <div class="">
                        <div class="content content-full" >
                            <div class="my-5 text-center">
                                <h2 class="h3 mb-4 invisible text-light" data-toggle="appear">#aprendeDesdeCasa</h2>
                                <a class="btn btn-rounded btn-primary px-4 py-2 " data-toggle="appear" data-class="animated bounceIn" href="{{url('/')}}/">Regresar a Inicio</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
                <!-- END Page Content -->
@endsection
@section('scripts')
    <script>

        function registrar() {

            $.ajax({
                data: $('#form-registrar').serialize(),
                url: "{{ URL::to('/') }}/api/registrar",
                type: "POST",
                success: function (data) {

                   $("#alert-reg").show();
                   $("#alert-reg").html("Ya estás registrado,Bienvenido/a!, Puede " +'<a href='+"{{ URL::to('/') }}/ingresar"+'>Iniciar sessión</a>');

                },
                error: function (data) {

                }
            });
        }
    </script>
    @endsection
