@extends('layouts.admin')

@section('content')
    <!-- Main Container -->
    <main id="main-container">
        <!-- Categories-->
        <div class="bg-body-light">
            <div class="content content-full">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                    <h1 class="flex-sm-fill h3 my-2">
                        Lista de Profesores <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Profesores creados, se puede editar,asignar materias y deshabilitar.</small>
                    </h1>
                    <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-alt">
                            <li class="breadcrumb-item">Ir a</li>
                            <li class="breadcrumb-item" aria-current="page">
                                <a class="link-fx" href="{{url('/')}}/admin/profesor/crear">Crear Profesor/a</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Categories-->
        <div class="content">
            <!-- Partial Table -->
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Tabla Profesores</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option">
                            <i class="si si-settings"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="table-responsive">

                        <table class="table table-bordered table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th style="width: 20%;">Correo</th>
                            <th class="d-none d-sm-table-cell" style="width: 15%;">Apodo</th>
                            <th class="d-none d-sm-table-cell" style="width: 15%;">Estado</th>
                            <th class="d-none d-md-table-cell text-center" style="width: 100px;">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($profesores as $profe)
                        <tr  id="fila-{{$profe->id}}">
                            <td class="font-w600 font-size-sm" id="name_profe_table-{{$profe->id}}">
                                {{$profe->user->name}}
                            </td>
                            <td class="d-none d-sm-table-cell font-size-sm" id="email_profe_table-{{$profe->id}}">{{$profe->user->email}}</td>
                            <td class="d-none d-md-table-cell" id="apodo_profe_table-{{$profe->id}}">
                                {{$profe->apodo}}
                            </td>
                            <td class="d-none d-md-table-cell" id="statu_profe_table-{{$profe->id}}">
                                {{$profe->statu == 1? 'Activo' : 'Inactivo'}}
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-block-slideright-materias" title="Materias" data-profe="{{$profe->id}}" onclick="asignarMateria(this)">
                                        <i class="fa fa-fw fa-book"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-block-slideright" title="Editar" data-profeid="{{$profe->id}}" onclick="getProfesor(this)">
                                        <i class="fa fa-fw fa-pencil-alt"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Borrar" data-profeid="{{$profe->id}}"   onclick="deleteProfesor(this)">
                                        <i class="fa fa-fw fa-times"></i>
                                    </button>
                                </div>
                            </td>


                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Partial Table -->
        </div>
        </div>
    </main>
    <!-- Slide Right Block Modal -->
    <div class="modal fade" id="modal-block-slideright" tabindex="-1" role="dialog" aria-labelledby="modal-block-slideright" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideright" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Editar Profesor</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <form class="js-validation" id="form_edit_profe" method="POST">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">Editar Profesor</h3>
                                </div>
                                {{--                //alert--}}
                                @include('components.alerts')
                                <div class="block-content block-content-full " id="block-modal">

                                @csrf
                                    <input id="id_profe" name="id" type="hidden">

                                    <div class="row items-push">
                                        <div class="col-lg-8 col-xl-10">
                                            <div class="form-group">
                                                <label for="val-username">Nombre</label>
                                                <input type="text" class="form-control text-left" id="name_edit" name="name" placeholder="Nombre" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="val-username">Email </label>
                                                <input type="email" class="form-control" id="email_edit" name="email" placeholder="Email" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="val-apodo">Apodo</label>
                                                <input type="text" class="form-control" id="apodo_edit" name="apodo" placeholder="Apodo">

                                            </div>

                                            <div class="form-group">
                                                <label for="val-skill">Estado</label>
                                                <select class="form-control" id="statu_edit" name="statu">
                                                    <option value="">Seleccione</option>
                                                    <option value="1">Activo</option>
                                                    <option value="2">Inactivo</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Regular -->

                                    <!-- Submit -->
                                    <div class="row items-push">
                                        <div class="col-lg-7 offset-lg-4">
                                            <button type="button" class="btn btn-primary" onclick="editProfesor()">Guarda</button>
                                        </div>
                                    </div>
                                    <!-- END Submit -->
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
                        {{--                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal"><i class="fa fa-check mr-1"></i>Ok</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Slide Right Block Modal -->

    <div class="modal fade" id="modal-block-slideright-materias" tabindex="-1" role="dialog" aria-labelledby="modal-block-slideright" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideright" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Materias Asignadas</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="block-header">
                            <h3 class="block-title">Tabla Materias asignada a profesor</h3>
                        </div>
                        <div class="table-responsive">

                        <table class="table table-bordered table-striped table-vcenter">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th >Grado</th>
                            </tr>
                            </thead>
                            <tbody  id="tabla_materia_profe">
{{--                            jquery--}}
                            </tbody>
                        </table>
                        </div>
                    </div>

                    <hr>
                    <div class="block-content font-size-sm text-center">
                        <form class="js-validation" id="form_materia_asignar" method="POST">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">Asignar nueva materia</h3>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="row items-push">
                                        <div class="col-lg-8 col-xl-10">
                                            <input type="hidden"  id="id_profesor_asig" name="id_profesor">

                                            <div class="form-group">
                                                <label for="val-skill">Materias<span class="text-danger">*</span></label>
                                                <select class="form-control"  name="id_materia" id="select-materia" >

                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- END Regular -->

                                    <!-- Submit -->
                                    <div class="row items-push">
                                        <div class="col-lg-7 offset-lg-4">
                                            <button type="button" class="btn btn-primary" onclick="addMateria()">Guarda</button>
                                        </div>
                                    </div>
                                    <!-- END Submit -->
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
                        {{--                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal"><i class="fa fa-check mr-1"></i>Ok</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        function asignarMateria(th) {
            var profe =$(th).data('profe');
            var token = $('#token-auth').data('token');
            $("#id_profesor_asig").val(profe);

            $.ajax({
                type: 'GET',
                url: "{{ URL::to('/') }}/api/admin/profe/materias/"+profe,
                headers: {
                    'Authorization': 'Bearer ' +token
                },
                success: function(data){
                    console.log(data);
                    $("#select-materia").html('<option value="">Seleccione</option>');
                    $("#tabla_materia_profe").html('');

                    $.each( data.materias, function( index, value ){

                        $("#tabla_materia_profe").append(
                            '<tr>' +
                            '<td class=" font-size-sm" id="materia">'+ value.nombre + '</td>' +
                            '<td class="d-sm-table-cell font-size-sm" id="materia">'+ value.grado +'</td>' +
                            '</tr>'
                        );
                    });
                    $.each( data.select, function( index, value ){

                        $("#select-materia").append('<option value=" '+ value.id +' ">' + value.nombre +'</option>');

                    });

                },
                error: function(data){
                    console.log(data);

                }

            });

        }

        function addMateria() {

            var token = $('#token-auth').data('token');

            $.ajax({
                data:$("#form_materia_asignar").serialize(),
                type: 'POST',
                url: "{{ URL::to('/') }}/api/admin/asignar/materia/",
                headers: {
                    'Authorization': 'Bearer ' +token
                },
                success: function(data){
                    $("#select-materia").html('<option value="">Seleccione</option>');
                    $("#tabla_materia_profe").html('');

                    $.each( data.materias, function( index, value ){

                        $("#tabla_materia_profe").append(
                            '<tr>' +
                            '<td class=" font-size-sm" id="materia">'+ value.nombre + '</td>' +
                            '<td class="d-sm-table-cell font-size-sm" id="materia">'+ value.grado +'</td>' +
                            '</tr>'
                        );
                    });
                    $.each( data.select, function( index, value ){

                        $("#select-materia").append('<option value=" '+ value.id +' ">' + value.nombre +'</option>');

                    });

                },
                error: function(data){

                }

            });

        }

        function getProfesor(th) {

            var id_profe =$(th).data('profeid');
            var nombre = $("#name_profe_table-"+id_profe).text();
            var email = $("#email_profe_table-"+id_profe).text();
            var apodo = $("#apodo_profe_table-"+id_profe).text();

            $("#name_edit").val(nombre);
            $("#email_edit").val(email);
            $("#apodo_edit").val(apodo);
            $("#id_profe").val(id_profe);

        }

        function editProfesor() {

            var token = $('#token-auth').data('token');

            $.ajax({
                data:$("#form_edit_profe").serialize(),
                type: 'POST',
                url: "{{ URL::to('/') }}/api/admin/profesor/editar",
                headers: {
                    'Authorization': 'Bearer ' +token
                },
                success: function(data){

                    $("#alert-ok2").html(data.msj);
                    $("#alert-ok2-div").show();
                    var id = data.profesor.id;
                    $("#apodo_profe_table-"+id).html(data.profesor.apodo);


                    if(data.profesor.statu == 1){
                        $("#statu_profe_table-"+id).html("Activo");
                    }else{
                        $("#statu_profe_table-"+id).html("Inactivo");
                    }
                    setTimeout(function() {
                        $("#alert-ok2-div").fadeOut();
                    },5000);

                },
                error: function(data){

                    $("#alert-ok2").html(data.msj);
                    $("#alert-ok2-div").show();

                    setTimeout(function() {
                        $("#alert-ok2-div").fadeOut();
                    },4000);

                }
            });

        }

        function deleteProfesor(th) {

            var id_profe = $(th).data('profeid');
            var token = $('#token-auth').data('token');
            var nombre = $("#name_profe_table-"+id_profe).text();

            Swal.fire({
                title: 'Quiere eliminar el profesor?',
                text: "Profesor "+ nombre +" será eliminado!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#fb3636',
                confirmButtonText: 'Si, eliminar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: 'GET',
                        url: "{{ URL::to('/') }}/api/admin/profesor/eliminar/"+id_profe,
                        headers: {
                            'Authorization': 'Bearer ' +token
                        },
                        success: function(data){
                            $("#fila-"+id_profe).hide();

                            Swal.fire(
                                'Eliminado!',
                                data.msj,
                            );

                        },
                        error: function(data){

                            Swal.fire(
                                'Error!',
                                data.responseJSON.msj,
                            );

                        }

                    });
                }
            });




        }
    </script>
    @endsection
