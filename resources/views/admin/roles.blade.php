@extends('layouts.admin')

@section('content')
    <!-- Main Container -->
    <main id="main-container">

        <!-- Hero -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                    <h1 class="flex-sm-fill h3 my-2">
                        Lista de Roles <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Los roles creados, se puede editar y eliminar.</small>
                    </h1>
                        <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-alt">
                                <li class="breadcrumb-item">Ir a</li>
                                <li class="breadcrumb-item" aria-current="page">
                                    <a class="link-fx" href="{{url('/')}}/admin/roles/crear">Crear Role</a>
                                </li>
                            </ol>
                        </nav>
                </div>
            </div>
        </div>
        <!-- Page Content -->
        <div class="content">
            <!-- Full Table -->
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Tabla Roles</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option">
                            <i class="si si-settings"></i>
                        </button>
                    </div>
                </div>


                <div class="block-content">
                        <div class="table-responsive">

                            <table class="table table-bordered table-striped table-vcenter">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th style="width: 30%;">Slug</th>
                                        <th class="d-sm-table-cell" style="width: 15%;">Estado</th>
                                        <th class="d-md-table-cell text-center" style="width: 100px;">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $rol)
                                    <tr id="rol-tabla-{{$rol->id}}">

                                        <td class="font-w600 font-size-sm" id="name-tabla-{{$rol->id}}">
                                           {{$rol->name}}
                                        </td>
                                        <td class="d-sm-table-cell " id="slug-tabla-{{$rol->id}}">{{$rol->slug}}</td>
                                        <td class="d-md-table-cell font-size-sm" id="statu_tabla-{{$rol->id}}">
                                            {{$rol->statu == 1? 'Activo' :'Inactivo'}}
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-block-slideright" title="Editar" data-role="{{$rol}}" onclick="getRol(this)">
                                                    <i class="fa fa-fw fa-pencil-alt"></i>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Borrar"  data-idrol="{{$rol->id}}" onclick="deleteRol(this)">
                                                    <i class="fa fa-fw fa-times"></i>
                                                </button>
                                            </div>
                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            <!-- END Partial Table -->
        </div>
        </div>
    </main>
    <!-- Slide Right Block Modal -->
    <div class="modal fade" id="modal-block-slideright" tabindex="-1" role="dialog" aria-labelledby="modal-block-slideright" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideright" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Editar Rol</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>


                    <div class="block-content font-size-sm">
                        <form class="js-validation"  id="form_editar_rol" action="#" method="POST">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">Editar Rol</h3>
                                </div>

                                {{--                //alert--}}
                                @include('components.alerts')
                                <div class="block-content block-content-full " id="block-modal">

                                    @csrf
                                    <input id="id_rol" name="id" type="hidden">
                                        <div class="row items-push">
                                            <div class="col-lg-8 col-xl-10">
                                                <div class="form-group">
                                                    <label for="val-username">Nombre <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="name_rol" name="name" placeholder="Nombre">
                                                </div>
                                                <div class="form-group">
                                                    <label for="val-email">Slug <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" id="slug_rol" name="slug" placeholder="Slug">
                                                </div>

                                                <div class="form-group">
                                                    <label for="val-skill">Estado<span class="text-danger">*</span></label>
                                                    <select class="form-control" id="statu_rol" name="statu">
                                                        <option value="">Seleccione</option>
                                                        <option value="1">Activo</option>
                                                        <option value="2">Inactivo</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- END Regular -->

                                        <!-- Submit -->
                                        <div class="row items-push">
                                            <div class="col-lg-7 offset-lg-4">
                                                <button type="button" class="btn btn-primary" onclick="editarRol()">Guarda</button>
                                            </div>
                                        </div>
                                        <!-- END Submit -->
                                    </div>

                            </div>
                        </form>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
{{--                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal"><i class="fa fa-check mr-1"></i>Ok</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Slide Right Block Modal -->
@endsection
@section('scripts')
    <script>

        /**
         *
         * */
        function getRol(rol) {
            var rol =$(rol).data('role');

            $("#name_rol").val(rol.name);
            $("#slug_rol").val(rol.slug);
            $("#id_rol").val(rol.id);
        }

        /**
         * Editar Rol
         **/
        function editarRol() {

            var token = $('#token-auth').data('token');

            $.ajax({
                type: 'POST',
                data: $('#form_editar_rol').serialize(),
                url: "{{ URL::to('/') }}/api/admin/role/editar",

                headers: {
                    'Authorization': 'Bearer ' +token
                },
                success: function(data){

                    $("#alert-ok2").html(data.msj);
                    $("#alert-ok2-div").show();
                    var id = data.rol.id;
                    $("#rol_tabla-"+id).html(data.rol.name);
                    $("#name_tabla-"+id).html(data.rol.slug);

                    if(data.rol.statu == 1){
                        $("#statu_tabla-"+id).html("Activo");
                    }else{
                        $("#statu_tabla-"+id).html("Inactivo");
                    }
                    setTimeout(function() {
                        $("#alert-ok2-div").fadeOut();
                    },5000);

                },
                error: function(data){

                    $("#alert-ok2").html(data.msj);
                    $("#alert-ok2-div").show();

                    setTimeout(function() {
                        $("#alert-ok2-div").fadeOut();
                    },4000);

                }

            });
        }

        /**
         * elimina el rol
         * @param id
         */
        function deleteRol(id) {

            var id_rol = $(id).data('idrol');
            var token = $('#token-auth').data('token');
            var name_rol =$("#name-tabla-"+id_rol).text();

            Swal.fire({
                title: 'Quiere eliminar el Rol?',
                text: "Rol "+name_rol+" será deshabilitar!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#fb3636',
                confirmButtonText: 'Si, deshabilitar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: 'GET',
                        url: "{{ URL::to('/') }}/api/admin/role/eliminar/"+id_rol,
                        headers: {
                            'Authorization': 'Bearer ' +token
                        },
                        success: function(data){
                            $("#statu_tabla-"+id_rol).html("Inactivo");

                            Swal.fire(
                                'Deshabilitado!',
                                data.msj,
                            );

                        },
                        error: function(data){

                            Swal.fire(
                                'Error!',
                                data.responseJSON.msj,
                            );

                        }

                    });


                }
            });



        }
    </script>
@endsection
