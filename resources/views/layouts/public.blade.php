<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title> ColeEnCasa @yield('title')</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
        <link rel="stylesheet" id="css-main" href=" {{ asset('css/oneui.min.css')}}">
        <link rel="stylesheet" id="css-main" href=" {{ asset('css/bootstrap.min.css')}}">
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{ asset('css/slider/slick.css')}}">

        <link rel="stylesheet" href="{{ asset('css/slider/slick-theme.css')}}">

    </head>
    <body>

    @section('sidebar')

        @show
            <!-- Main Container -->
            <main id="main-container">
                @yield('content')
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="bg-body-light ">
                <div class="content py-3">
                    <div class="row font-size-sm">
                        <div class="col-sm-6 order-sm-1 py-1 ">
                            <a class="font-w400 text-info " href="#" target="_blank">Desarrollo web <strong>ArSoft</strong> </a> &copy; 2020</span>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->



        <!-- END Page Container -->

        <script src="{{asset('/js/oneui.core.min.js')}}"></script>
        <script src="{{asset('/js/oneui.app.min.js')}}"></script>
{{--        <script src="{{asset('/js/bootstrap/bootstrap.js')}}"></script>--}}

    @yield('scripts')
    </body>
</html>
