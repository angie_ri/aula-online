<?php $__env->startSection('content'); ?>
    <!-- Main Container -->
    <main id="main-container">

        <!-- Hero -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                    <h1 class="flex-sm-fill h3 my-2">
                        Lista de Usuario <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Los usuarios registrados, se puede editar rol y/o estado.</small>
                    </h1>

                </div>
            </div>
        </div>
        <!-- END Hero -->

        <!-- Page Content -->
        <div class="content">
            <!-- Full Table -->
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Tabla Usuarios</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option">
                            <i class="si si-settings"></i>
                        </button>
                    </div>
                </div>
                <?php echo $__env->make('components.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="block-content">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-vcenter">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th style="width: 30%;">Email</th>
                                <th style="width: 15%;">Rol</th>
                                <th style="width: 15%;">Estado</th>
                                <th class="text-center" style="width: 100px;">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr id="fila-<?php echo e($user->id); ?>">
                                <td class="font-w600 font-size-sm" id="name-table-<?php echo e($user->id); ?>">
                                    <?php echo e($user->name); ?>

                                </td>
                                <td class="font-size-sm"><?php echo e($user->email); ?></td>
                                <td id="rol_tabla-<?php echo e($user->id); ?>"><?php echo e($user->rolUser ? $user->rolUser->role->name : ''); ?></td>
                                </td>
                                <td id="statu_tabla-<?php echo e($user->id); ?>"><?php echo e($user->statu == 1? 'Activo':'Inactivo'); ?></td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#modal-block-slideright" data-user="<?php echo e($user); ?>" title="Editar" onclick="getUser(this)">
                                            <i class="fa fa-fw fa-pencil-alt"></i>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Bloquear" data-iduser="<?php echo e($user->id); ?>" onclick="deleteUser(this)">
                                            <i class="fa fa-fw fa-times"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Full Table -->
        </div>
        <!-- END Page Content -->

    </main>
    <!-- END Main Container -->

    <!-- Slide Right Block Modal -->
    <div class="modal fade" id="modal-block-slideright" tabindex="-1" role="dialog" aria-labelledby="modal-block-slideright" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideright" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Editar Usuario</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <form class="js-validation"  id="form_editar_user" method="POST">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">Editar Usuario</h3>
                                </div>
                                <?php echo $__env->make('components.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <div class="block-content block-content-full " id="block-modal">


                                    <div class="row items-push ">
                                        <div class="col-lg-8 col-xl-10">
                                            <?php echo csrf_field(); ?>
                                            <input id="id_user" name="id" type="hidden">
                                            <div class="form-group">
                                                <label for="val-username">Nombre</label>
                                                <input type="text" class="form-control" id="name_user" name="name" placeholder="Nombre" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="val-username">Email</label>
                                                <input type="email" class="form-control" id="email_user" name="email" placeholder="Nombre" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="val-skill">Rol</label>
                                                <select class="form-control" id="rol_user" name="rol">
                                                    <option value="">Seleccione</option>
                                                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rol): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($rol->id); ?>"><?php echo e($rol->name); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="val-skill">Estado</label>
                                                <select class="form-control" id="statu_user" name="statu">
                                                    <option value="">Seleccione</option>
                                                        <option value="1">Activo</option>
                                                        <option value="2">Inactivo</option>

                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- END Regular -->

                                    <!-- Submit -->
                                    <div class="row items-push">
                                        <div class="col-lg-7 offset-lg-4">
                                            <button type="button" class="btn btn-primary"   onclick="editarUser()">Guarda</button>
                                        </div>
                                    </div>
                                    <!-- END Submit -->
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <
    <!-- END Slide Right Block Modal -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>
        function getUser(user) {
            var user =$(user).data('user');

            $("#name_user").val(user.name);
            $("#email_user").val(user.email);
            $("#id_user").val(user.id);
        }

        function editarUser() {
            var token = $('#token-auth').data('token');
            var id =$("#id_user").val();

            $.ajax({
                type: 'POST',
                data: $('#form_editar_user').serialize(),
                url: "<?php echo e(URL::to('/')); ?>/api/admin/usuario/editar",

                headers: {
                    'Authorization': 'Bearer ' +token
                },
                success: function(data){

                    $("#alert-ok").html(data.msj);
                    $("#alert-ok-div").show();


                    $("#rol_tabla-"+id).html(data.rol);

                    if(data.statu == 1)
                    {
                        $("#statu_tabla-"+id).html("Activo");

                    }else{
                        $("#statu_tabla-"+id).html("Inactivo");

                    }

                },
                error: function(data){
                    $("#alert-error").html(data.msj);
                    $("#alert-error-div").show();
                    //Cuando la interacción retorne un error, se ejecutará esto.
                }

        });
        }

        function deleteUser(id) {

            var id_user = $(id).data('iduser');
            var token = $('#token-auth').data('token');
            var name = $("#name-table-"+id_user).text();

            Swal.fire({
                title: 'Quiere bloquear el usuario?',
                text: "Usuario "+name+" será bloqueado!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#fb3636',
                confirmButtonText: 'Si, bloquear!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: 'GET',
                        url: "<?php echo e(URL::to('/')); ?>/api/admin/usuario/eliminar/"+id_user,
                        headers: {
                            'Authorization': 'Bearer ' +token
                        },
                        success: function(data){

                            $("#fila-"+id_user).hide();
                            Swal.fire(
                                'Bloqueado!',
                                data.msj,
                            );

                        },
                        error: function(data){

                            Swal.fire(
                                'Error!',
                                data.responseJSON.msj,
                            );
                            //Cuando la interacción retorne un error, se ejecutará esto.
                        }

                    });
                }
            });

            
            
            
            
            
            
            

            
            
            

            
            
            


            
            

            
            
            
            
            
            
            

            


        }
    </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\proyectos\proyectos PHP\matedivertida1\resources\views/admin/users.blade.php ENDPATH**/ ?>