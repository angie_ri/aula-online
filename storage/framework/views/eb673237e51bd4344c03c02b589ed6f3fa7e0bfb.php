<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <title> MateDivertida <?php echo $__env->yieldContent('title'); ?></title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
        <link rel="stylesheet" id="css-main" href=" <?php echo e(asset('css/oneui.min.css')); ?>">

    </head>
    <body>

    <?php $__env->startSection('sidebar'); ?>

        <?php echo $__env->yieldSection(); ?>
            <!-- Main Container -->
            <main id="main-container">
                <?php echo $__env->yieldContent('content'); ?>
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="bg-body-light">
                <div class="content py-3">
                    <div class="row font-size-sm">
                        <div class="col-sm-6 order-sm-1 py-1 text-left">
                            <a class="font-w600" href="https://1.envato.market/xWy" target="_blank"><strong>ArSoft</strong> </a> &copy; <span data-toggle="year-copy"></span>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->



        <!-- END Page Container -->




    <?php echo $__env->yieldContent('scripts'); ?>
    </body>
</html>
<?php /**PATH E:\proyectos\PHP\matedivertida\resources\views/layouts/public.blade.php ENDPATH**/ ?>