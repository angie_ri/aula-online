<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title> MateDivertida <?php echo $__env->yieldContent('title'); ?></title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    <link rel="stylesheet" id="css-main" href=" <?php echo e(asset('css/oneui.min.css')); ?>">
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('css/slider/slick.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('css/slider/slick-theme.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/modern.css')); ?>">

</head>
<body>

<div id="page-container" class="sidebar-o sidebar-dark sidebar-mini enable-page-overlay side-scroll page-header-fixed">

    <nav id="sidebar" aria-label="Main Navigation" >
        <!-- Side Header -->
        <div class="content-header bg-white-5">
            <!-- Logo -->
            <a class="font-w600 text-dual" href="#">  Sal
                <i class="fa fa-circle-notch text-modern"></i>
                <span class="smini-hide">
                    <span class="font-w700 font-size-h5">nline</span>
                        </span>
            </a>
            <!-- END Logo -->
        </div>
        <!-- END Side Header -->

        <!-- Side Navigation -->
        <div class="content-side content-side-full">
            <ul class="nav-main">
                <li class="nav-main-item  ">
                    <a class="nav-main-link <?php echo e(request()->is('clases/sala') ? 'active' :''); ?>" href="<?php echo e(url('/')); ?>/clases/sala">
                        <i class="nav-main-link-icon far fa-id-card"></i>
                        <span class="nav-main-link-name">Tablero</span>
                     </a>
                 </li>
         <li class="nav-main-heading">Aula</li>
         <li class="nav-main-item <?php echo e(request()->is('/hh') ? 'active' :''); ?>">
             <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                 <i class="nav-main-link-icon far fa-list-alt"></i>
                 <span class="nav-main-link-name">Materias</span>
             </a>
             <ul class="nav-main-submenu">
                 <li class="nav-main-item">
                     <a class="nav-main-link <?php echo e(request()->is('cla') ? 'active' :''); ?>" href="">
                         <span class="nav-main-link-name">Ejm1 Algebra</span>
                     </a>
                 </li>
             </ul>
         </li>
         <li class="">
             <a class="nav-main-link <?php echo e(request()->is('cla') ? 'active' :''); ?> nav-main-link" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                 <i class="nav-main-link-icon si si-note"></i>
                 <span class="nav-main-link-name">Tareas</span>

             </a>
         </li>
         <li class="nav-main-item">
             <a class="nav-main-link <?php echo e(request()->is('cla') ? 'active' :''); ?> nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                 <i class="nav-main-link-icon  si si-users"></i>
                 <span class="nav-main-link-name">Profesores</span>
             </a>

         </li>
         <li class="nav-main-heading">Alumno</li>
         <li class="nav-main-item<?php echo e(request()->is('examples/*') ? 'active' :''); ?>">
             <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                 <i class="nav-main-link-icon far fa-user-circle"></i>
                 <span class="nav-main-link-name">Perfil</span>
             </a>
             <ul class="nav-main-submenu">
                 <li class="nav-main-item">
                     <a class="nav-main-link<?php echo e(request()->is('cala') ? 'active' :''); ?>" href="be_blocks_styles.html">
                         <span class="nav-main-link-name">Editar Perfil</span>
                     </a>
                 </li>
             </ul>
         </li>


















































            </ul>
        </div>

        <!-- END Side Navigation -->
    </nav>
    <!-- END Sidebar -->


    <!-- Header -->
    <header id="page-header "  class="bg-modern">
        <!-- Header Content -->
        <div class="content-header  ml-lg-2 mr-lg-2">
            <!-- Left Section -->
            <div class="d-flex align-items-center">
                <!-- Toggle Sidebar -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout()-->
                <button type="button" class="btn btn-sm btn-dual mr-2 d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
                    <i class="fa fa-fw fa-bars"></i>
                </button>
                <!-- END Toggle Sidebar -->

                <!-- Toggle Mini Sidebar -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout()-->
                <button type="button" class="btn btn-sm btn-dual mr-2 d-none d-lg-inline-block" data-toggle="layout" data-action="sidebar_mini_toggle">
                    <i class="fa fa-fw fa-ellipsis-v"></i>
                </button>
                <!-- END Toggle Mini Sidebar -->

                <!-- Apps Modal -->
                <!-- Opens the Apps modal found at the bottom of the page, after footer’s markup -->
                <button type="button" class="btn btn-sm btn-dual mr-2" data-toggle="modal" data-target="#one-modal-apps">
                    <i class="si si-grid"></i>
                </button>
                <!-- END Apps Modal -->

                <!-- Open Search Section (visible on smaller screens) -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-sm btn-dual d-sm-none" data-toggle="layout" data-action="header_search_on">
                    <i class="si si-magnifier"></i>
                </button>
                <!-- END Open Search Section -->

                <!-- Search Form (visible on larger screens) -->
                <form class="d-none d-sm-inline-block" action="#" method="POST">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control form-control-alt" placeholder="Buscar.." id="page-header-search-input2" name="page-header-search-input2">
                        <div class="input-group-append">
                                    <span class="input-group-text bg-body border-0">
                                        <i class="si si-magnifier"></i>
                                    </span>
                        </div>
                    </div>
                </form>
                <!-- END Search Form -->
            </div>
            <!-- END Left Section -->

            <!-- Right Section -->
            <div class="d-flex align-items-center">
                <!-- User Dropdown -->
                <div class="dropdown d-inline-block ml-2">
                    <button type="button" class="btn btn-sm btn-dual" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="rounded" src="<?php echo e(asset('imagen/1.png')); ?>" style="width: 18px;">
                        <span class="d-none d-sm-inline-block ml-1"><?php echo e($auth->name); ?> </span>
                        <i class="fa fa-fw fa-angle-down d-none d-sm-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right p-0 border-0 font-size-sm" aria-labelledby="page-header-user-dropdown">
                        <div class="p-3 text-center bg-primary">
                            <img class="img-avatar img-avatar48 img-avatar-thumb" src="<?php echo e(asset('imagen/1.png')); ?>" alt="">
                        </div>
                        <div class="p-2">
                            <h5 class="dropdown-header text-uppercase">Opciones</h5>
                            <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">
                                <span>Mensajes</span>
                                <span>
                                    <span class="badge badge-pill badge-primary">3</span>
                                    <i class="si si-envelope-open ml-1"></i>
                                </span>
                            </a>
                            <a class="dropdown-item d-flex align-items-center justify-content-between" href="#">
                                <span>Perfil</span>
                                <span>
                                <span class="badge badge-pill badge-success">1</span>
                                <i class="si si-user ml-1"></i>
                            </span>
                            </a>

                            <div role="separator" class="dropdown-divider"></div>
                            <h5 class="dropdown-header text-uppercase">Acciones</h5>
                            <a class="dropdown-item d-flex align-items-center justify-content-between"  href="<?php echo e(URL::to('/')); ?>/logout">
                                <span>Salir</span>
                                <i class="si si-logout ml-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END User Dropdown -->

                <!-- Notifications Dropdown -->
                <div class="dropdown d-inline-block ml-2">
                    <button type="button" class="btn btn-sm btn-dual" id="page-header-notifications-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="si si-bell"></i>
                        <span class="badge badge-primary badge-pill">6</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0 border-0 font-size-sm" aria-labelledby="page-header-notifications-dropdown">
                        <div class="p-2 bg-primary text-center">
                            <h5 class="dropdown-header text-uppercase text-white">Notificationes</h5>
                        </div>
                        <ul class="nav-items mb-0">

                            <li>
                                <a class="text-dark media py-2" href="javascript:void(0)">
                                    <div class="mr-2 ml-3">
                                        <i class="fa fa-fw fa-check-circle text-success"></i>
                                    </div>
                                    <div class="media-body pr-2">
                                        <div class="font-w600">You have a new follower</div>
                                        <small class="text-muted">42 min ago</small>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <div class="p-2 border-top">
                            <a class="btn btn-sm btn-light btn-block text-center" href="javascript:void(0)">
                                <i class="fa fa-fw fa-arrow-down mr-1"></i> Leer mas...
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END Notifications Dropdown -->

            </div>
            <!-- END Right Section -->
        </div>
        <!-- END Header Content -->

    </header>

    <!-- Main Container -->
    <main id="main-container">
        <?php echo $__env->yieldContent('content'); ?>

    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="bg-body-light">
        <div class="content py-3">
            <div class="row font-size-sm">
                <div class="col-sm-6 order-sm-1 py-1 text-left ">
                    <a class="font-w600" href="https://1.envato.market/xWy" target="_blank"><strong>ArSoft</strong> </a> &copy; <span data-toggle="year-copy"></span>
                </div>
            </div>
        </div>
    </footer>
    <!-- END Footer -->

</div>



<!-- END Page Container -->

<script src="<?php echo e(asset('/js/oneui.core.min.js')); ?>"></script>
<script src="<?php echo e(asset('/js/oneui.app.min.js')); ?>"></script>

<script>


</script>
<?php echo $__env->yieldContent('scripts'); ?>



</body>
</html>
<?php /**PATH E:\proyectos\proyectos PHP\matedivertida1\resources\views/layouts/app.blade.php ENDPATH**/ ?>