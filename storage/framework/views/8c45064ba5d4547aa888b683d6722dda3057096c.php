<?php $__env->startSection('content'); ?>

    <main id="main-container">
        <!-- Categories-->
        <div class="content content-boxed overflow-hidden">

            <!-- Page Content -->
            <div class="content">
                <!-- jQuery Validation (.js-validation class is initialized in js/pages/be_forms_validation.min.js which was auto compiled from _es6/pages/be_forms_validation.js) -->
                <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                <form class="js-validation" action="#" method="POST">
                    <div class="block">
                        <div class="block-header">
                            <h3 class="block-title">Editar Rol</h3>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="">
                                <!-- Regular -->

                                <div class="row items-push">
                                    <div class="col-lg-4">
                                        <p class="font-size-sm text-muted">
                                            Username, email and password validation made easy for your login/register forms
                                        </p>
                                    </div>
                                    <div class="col-lg-8 col-xl-5">
                                        <div class="form-group">
                                            <label for="val-username">Nombre <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Nombre">
                                        </div>
                                        <div class="form-group">
                                            <label for="val-email">Slug <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug">
                                        </div>
                                        <div class="form-group">
                                            <label for="val-skill">Estado<span class="text-danger">*</span></label>
                                            <select class="form-control"  name="statu">
                                                <option value="">Seleccione</option>
                                                <option value="1">Activo</option>
                                                <option value="2">Inactivo</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <!-- END Regular -->

                                <!-- Submit -->
                                <div class="row items-push">
                                    <div class="col-lg-7 offset-lg-4">
                                        <button type="submit" class="btn btn-primary">Guarda</button>
                                    </div>
                                </div>
                                <!-- END Submit -->
                            </div>
                        </div>
                    </div>
                </form>
                <!-- jQuery Validation -->
            </div>
            <!-- END Page Content -->
        </div>

    </main>
    <!-- END Main Container -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\proyectos\proyectos PHP\matedivertida1\resources\views/admin/new_user.blade.php ENDPATH**/ ?>