<!-- Main Container -->

    <!-- Hero Content -->
    <div class="bg-image" style="background-image: url('<?php echo e(asset('/imagen/fondo3.png')); ?>')">
        <div class="bg-black-50">
            <div class="content content-full overflow-hidden">
                <div class="mt-7 mb-5 text-center">
                    <h1 class="h2 text-white mb-2 invisible" data-toggle="appear" data-class="animated fadeInDown">MATEDIVERTIDA con Teacher Juan</h1>
                    <h2 class="h4 font-w400 text-white-75 invisible" data-toggle="appear" data-class="animated fadeInDown">Clases virtuales de Matemáticas.</h2>
                    <a class="btn btn-rounded btn-success px-4 py-2 invisible" data-toggle="appear" data-class="animated zoomIn" href="javascript:void(0)">Contacto +51 978401020</a>
                    <h2 class="h4 font-w400 text-white-75 invisible mt-2" data-toggle="appear" data-class="animated fadeInDown">Primaria - Secundaria</h2>

                </div>
            </div>
        </div>
    </div>
    <!-- END Hero Content -->

    <!-- Page Content -->
    <div class="content content-boxed">
        <div class="row row-deck py-4 m-auto">
            <!-- Course -->
            <div class="col-md-6 col-lg-4 col-xl-3">
                <a class="block block-rounded block-link-pop" href="be_pages_elearning_course.html">
                    <div class="block-content block-content-full text-center " style="background-image: url('<?php echo e(asset('/imagen/juan.jpg')); ?>')">
                        <div class="item item-2x item-circle bg-white-10 py-3 my-3 mx-auto invisible" data-toggle="appear" data-offset="50" data-class="animated fadeIn">

                        </div>
                        <div class="font-size-sm text-white-75">
                            <em></em> &bull;
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <h4 class="mb-1">Aprende Matemáticas de manera divertida.</h4>
                        <div class="font-size-sm text-muted"></div>
                    </div>
                </a>
            </div>
            <!-- END Course -->

            <!-- Course -->
            <div class="col-md-6 col-lg-4 col-xl-3">
                <a class="block block-rounded block-link-pop" href="be_pages_elearning_course.html">
                    <div class="block-content block-content-full text-center bg-flat" >
                        <div class="item item-2x item-circle bg-white-10 py-3 my-3 mx-auto invisible" data-toggle="appear" data-offset="50" data-class="animated fadeIn">
                            <i class="si si-camcorder fa-2x text-white-75"></i>
                        </div>
                        <div class="font-size-sm text-white-75">
                            <em></em> &bull;
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <h4 class="mb-1"></h4>
                    </div>
                </a>
            </div>
            <!-- END Course -->

            <!-- Course -->
            <div class="col-md-6 col-lg-4 col-xl-3">
                <a class="block block-rounded block-link-pop" href="be_pages_elearning_course.html">
                    <div class="block-content block-content-full text-center bg-amethyst">
                        <div class="item item-2x item-circle bg-white-10 py-3 my-3 mx-auto invisible" data-toggle="appear" data-offset="50" data-class="animated fadeIn">

                        </div>
                        <div class="font-size-sm text-white-75">
                            <em></em> &bull;
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <h4 class="mb-1"></h4>
                        <div class="font-size-sm text-muted"></div>
                    </div>
                </a>
            </div>
            <!-- END Course -->

            <!-- Course -->
            <div class="col-md-6 col-lg-4 col-xl-3">
                <a class="block block-rounded block-link-pop" href="be_pages_elearning_course.html">
                    <div class="block-content block-content-full text-center bg-city">
                        <div class="item item-2x item-circle bg-white-10 py-3 my-3 mx-auto invisible" data-toggle="appear" data-offset="50" data-class="animated fadeIn">

                        </div>
                        <div class="font-size-sm text-white-75">
                            <em></em> &bull;
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <h4 class="mb-1">Aprende Matemáticas de manera divertida.</h4>
                        <div class="font-size-sm text-muted"></div>
                    </div>
                </a>
            </div>
            <!-- END Course -->
        </div>
    </div>
    <!-- END Page Content -->

    <!-- Instructors -->
    <div class="bg-image" style="background-image: url('<?php echo e(asset('/imagen/mate1.png')); ?>');">
        <div class="bg-primary-dark-op py-5">
            <div class="content content-full content-boxed text-center">
                <h2 class="font-w400 text-white mb-2 invisible" data-toggle="appear" data-offset="50" data-class="animated fadeInDown">Aprende desde casa con Teacher Juan Peredo</h2>
                <h3 class="h4 font-w400 text-white-75 invisible" data-toggle="appear" data-timeout="300" data-class="animated fadeInDown">Puedes aprender matemáticas de manera divertida.</h3>
                <div class="row items-push mt-5">
                    <div class="col-md invisible" data-toggle="appear" data-offset="-150" data-timeout="150" data-class="animated fadeInRight">
                        <img class="img-avatar img-avatar-thumb" src="assets/media/avatars/avatar1.jpg" alt="">
                        <div class="font-size-lg text-white mt-3">Juan Peredo</div>
                        <div class="font-size-sm text-white-50">Profesor de Matemáticas</div>
                        <a class="btn btn-rounded btn-success px-4 py-2 invisible mt-3" data-toggle="appear" data-class="animated bounceIn" href="javascript:void(0)">Contacto +51 978401020</a>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END Instructors -->

    <!-- Get Started -->
    <div class="bg-body-dark">
        <div class="content content-full">
            <div class="my-5 text-center">
                <h3 class="h4 mb-4 invisible" data-toggle="appear">#aprendeDesdeCasa</h3>
                <a class="btn btn-rounded btn-success px-4 py-2 invisible" data-toggle="appear" data-class="animated bounceIn" href="<?php echo e(url('/')); ?>/ingresar">Ingresar</a>
            </div>
        </div>
    </div>
    <!-- END Get Started -->


<!-- END Main Container -->

<?php echo $__env->make('layouts.public', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\proyectos\PHP\matedivertida\resources\views/inicio.blade.php ENDPATH**/ ?>