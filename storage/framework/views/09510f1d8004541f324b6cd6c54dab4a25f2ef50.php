<?php $__env->startSection('content'); ?>
    <!-- Main Container -->
    <main id="main-container">

        <!-- Categories-->
        <div class="content content-boxed overflow-hidden">
            <div class="row">
                <div class="col-sm-6 col-md-3 invisible" data-toggle="appear" data-class="animated fadeInDown">
                    <a class="block block-bordered block-rounded block-link-shadow" href="<?php echo e(url('/')); ?>/admin/usuario/lista">
                        <div class="block-content block-content-full border-bottom text-center">
                            <div class="py-3">
                                <i class="si si-user fa-2x text-amethyst"></i>
                            </div>
                        </div>
                        <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                            <span class="font-w600 text-uppercase font-size-sm">Cuentas</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-3 invisible" data-toggle="appear" data-timeout="200" data-class="animated fadeInDown">
                    <a class="block block-bordered block-rounded block-link-shadow" href="<?php echo e(url('/')); ?>/admin/roles/lista" >
                        <div class="block-content block-content-full border-bottom text-center">
                            <div class="py-3">
                                <i class="fa fa-users-cog fa-2x text-city"></i>
                            </div>
                        </div>
                        <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                            <span class="font-w600 text-uppercase font-size-sm">Roles</span>

                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-3 invisible" data-toggle="appear" data-timeout="400" data-class="animated fadeInDown">
                    <a class="block block-bordered block-rounded block-link-shadow" href="<?php echo e(url('/')); ?>/admin/materias/lista">
                        <div class="block-content block-content-full border-bottom text-center">
                            <div class="py-3">
                                <i class="fa fa-list-alt fa-2x text-flat"></i>
                            </div>
                        </div>
                        <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                            <span class="font-w600 text-uppercase font-size-sm">Materias</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-md-3 invisible" data-toggle="appear" data-timeout="600" data-class="animated fadeInDown">
                    <a class="block block-bordered block-rounded block-link-shadow" href="<?php echo e(url('/')); ?>/admin/profesor/lista">
                        <div class="block-content block-content-full border-bottom text-center">
                            <div class="py-3">
                                <i class="fa fa-chalkboard-teacher fa-2x text-secondary"></i>
                            </div>
                        </div>
                        <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                            <span class="font-w600 text-uppercase font-size-sm">Profesores</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- END Categories -->

        <!-- Frequently Asked Questions -->
        <div class="content content-full">
            <div class="row justify-content-center">
                <div class="col-md-8 py-5">
                    <!-- Introduction -->
                    <h2 class="h3">Introducción</h2>
                    <div id="faq1" class="mb-5" role="tablist" aria-multiselectable="true">
                        <div class="block block-bordered mb-1">
                            <div class="block-header block-header-default" role="tab" id="faq1_h1">
                                <a class="text-muted" data-toggle="collapse" data-parent="#faq1" href="#faq1_q1" aria-expanded="true" aria-controls="faq1_q1">Bienvenido a nuesros servicios!</a>
                            </div>
                            <div id="faq1_q1" class="collapse show" role="tabpanel" aria-labelledby="faq1_h1" data-parent="#faq1">
                                <div class="block-content">
                                    <p>Esta aplicación ayuda a manejar clases virtuales.</p>
                                    <p>Brinda roles de profesores, alumnos o administradores entre otros.</p>
                                </div>
                            </div>
                        </div>
                        <div class="block block-bordered mb-1">
                            <div class="block-header block-header-default" role="tab" id="faq1_h2">
                                <a class="text-muted" data-toggle="collapse" data-parent="#faq1" href="#faq1_q2" aria-expanded="true" aria-controls="faq1_q2">Who are we?</a>
                            </div>
                            <div id="faq1_q2" class="collapse" role="tabpanel" aria-labelledby="faq1_h2" data-parent="#faq1">
                                <div class="block-content">
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="block block-bordered mb-1">
                            <div class="block-header block-header-default" role="tab" id="faq1_h3">
                                <a class="text-muted" data-toggle="collapse" data-parent="#faq1" href="#faq1_q3" aria-expanded="true" aria-controls="faq1_q3">What are our values?</a>
                            </div>
                            <div id="faq1_q3" class="collapse" role="tabpanel" aria-labelledby="faq1_h3" data-parent="#faq1">
                                <div class="block-content">
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Introduction -->

                    <!-- Functionality -->
                    <h2 class="h3">Funciones</h2>
                    <div id="faq2" class="mb-5" role="tablist" aria-multiselectable="true">
                        <div class="block block-bordered mb-1">
                            <div class="block-header block-header-default" role="tab" id="faq2_h1">
                                <a class="text-muted" data-toggle="collapse" data-parent="#faq2" href="#faq2_q1" aria-expanded="true" aria-controls="faq2_q1">What are the key features?</a>
                            </div>
                            <div id="faq2_q1" class="collapse" role="tabpanel" aria-labelledby="faq2_h1" data-parent="#faq2">
                                <div class="block-content">
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="block block-bordered mb-1">
                            <div class="block-header block-header-default" role="tab" id="faq2_h2">
                                <a class="text-muted" data-toggle="collapse" data-parent="#faq2" href="#faq2_q2" aria-expanded="true" aria-controls="faq2_q2">Does your App support mobile devices?</a>
                            </div>
                            <div id="faq2_q2" class="collapse" role="tabpanel" aria-labelledby="faq2_h2" data-parent="#faq2">
                                <div class="block-content">
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="block block-bordered mb-1">
                            <div class="block-header block-header-default" role="tab" id="faq2_h3">
                                <a class="text-muted" data-toggle="collapse" data-parent="#faq2" href="#faq2_q3" aria-expanded="true" aria-controls="faq2_q3">Why should I choose your service?</a>
                            </div>
                            <div id="faq2_q3" class="collapse" role="tabpanel" aria-labelledby="faq2_h3" data-parent="#faq2">
                                <div class="block-content">
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="block block-bordered mb-1">
                            <div class="block-header block-header-default" role="tab" id="faq2_h4">
                                <a class="text-muted" data-toggle="collapse" data-parent="#faq2" href="#faq2_q4" aria-expanded="true" aria-controls="faq2_q4">Is my data secure?</a>
                            </div>
                            <div id="faq2_q4" class="collapse" role="tabpanel" aria-labelledby="faq2_h4" data-parent="#faq2">
                                <div class="block-content">
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Functionality -->

                    <!-- Payments -->
                    <h2 class="h3">Comunicado</h2>
                    <div id="faq3" role="tablist" aria-multiselectable="true">
                        <div class="block block-bordered mb-1">
                            <div class="block-header block-header-default" role="tab" id="faq3_h1">
                                <a class="text-muted" data-toggle="collapse" data-parent="#faq3" href="#faq3_q1" aria-expanded="true" aria-controls="faq3_q1">Is there any free plan?</a>
                            </div>
                            <div id="faq3_q1" class="collapse" role="tabpanel" aria-labelledby="faq3_h1" data-parent="#faq3">
                                <div class="block-content">
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="block block-bordered mb-1">
                            <div class="block-header block-header-default" role="tab" id="faq3_h2">
                                <a class="text-muted" data-toggle="collapse" data-parent="#faq3" href="#faq3_q2" aria-expanded="true" aria-controls="faq3_q2">What are the available payment options?</a>
                            </div>
                            <div id="faq3_q2" class="collapse" role="tabpanel" aria-labelledby="faq3_h2" data-parent="#faq3">
                                <div class="block-content">
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="block block-bordered mb-1">
                            <div class="block-header block-header-default" role="tab" id="faq3_h3">
                                <a class="text-muted" data-toggle="collapse" data-parent="#faq3" href="#faq3_q3" aria-expanded="true" aria-controls="faq3_q3">What are the available payment options?</a>
                            </div>
                            <div id="faq3_q3" class="collapse" role="tabpanel" aria-labelledby="faq3_h3" data-parent="#faq3">
                                <div class="block-content">
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                    <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Payments -->
                </div>
            </div>
        </div>
        <!-- END Frequently Asked Questions -->


    </main>
    <!-- END Main Container -->

    <?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\proyectos\proyectos PHP\Develop\matedivertida1\resources\views/admin/index.blade.php ENDPATH**/ ?>