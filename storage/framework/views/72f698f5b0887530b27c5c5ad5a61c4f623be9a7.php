
<div class="alert alert-success d-flex align-items-center m-2 " id="alert-ok-div" role="alert" style="display:none !important;">

    <div class="flex-fill ml-3">
        <p class="mb-0 text-center" id="alert-ok">Operación exitosa!</p>
    </div>
    <div class="flex-00-auto">
        <i class="fa fa-fw fa-check"></i>
    </div>
</div>
<div class="alert alert-danger d-flex align-items-center justify-content-between m-2"  id="alert-error-div" role="alert" style="display: none !important;">
    <div class="flex-fill mr-3">
        <p class="mb-0 text-center" id="alert-error">Se produjo un error! </p>
    </div>
    <div class="flex-00-auto">
        <i class="fa fa-fw fa-times-circle"></i>
    </div>
</div>
<div class="alert alert-success alert-dismissable ml-5 mr-5" id="alert-ok2-div" role="alert" style="display: none!important;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h3 class="alert-heading font-w300 my-2">Exitos!</h3>
    <p class="mb-0 text-center" id="alert-ok2"></p>
</div>
<div class="alert alert-danger alert-dismissable ml-5 mr-5" id="alert-error2-div" role="alert" style="display: none!important;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h3 class="alert-heading font-w300 my-2">Error!</h3>
    <p class="mb-0 text-center" id="alert-error2"></p>
</div>
<?php /**PATH E:\proyectos\proyectos PHP\matedivertida1\resources\views/components/alerts.blade.php ENDPATH**/ ?>