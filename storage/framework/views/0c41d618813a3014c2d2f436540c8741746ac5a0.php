<?php $__env->startSection('content'); ?>

<main id="main-container">
    <!-- Categories-->
    <div class="content content-boxed overflow-hidden">

    <!-- Page Content -->
    <div class="content">
        <!-- jQuery Validation (.js-validation class is initialized in js/pages/be_forms_validation.min.js which was auto compiled from _es6/pages/be_forms_validation.js) -->
        <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
        <form class="js-validation" id="form-rol" method="POST">


            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Nuevo Rol</h3>
                </div>

                <?php echo $__env->make('components.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                <div class="block-content block-content-full">
                    <div class="">
                        <!-- Regular -->

                        <div class="row items-push">
                            <div class="col-lg-4">
                                <p class="font-size-sm text-muted">
                                    Username, email and password validation made easy for your login/register forms
                                </p>
                            </div>
                            <div class="col-lg-8 col-xl-5">
                                <div class="form-group">
                                    <label for="val-username">Nombre <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Nombre">
                                </div>
                                <div class="form-group">
                                    <label for="val-email">Slug <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug">
                                </div>
                                <div class="form-group">
                                    <label for="val-skill">Estado<span class="text-danger">*</span></label>
                                    <select class="form-control"  name="statu">
                                        <option value="">Seleccione</option>
                                        <option value="1">Activo</option>
                                        <option value="2">Inactivo</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <!-- END Regular -->

                        <!-- Submit -->
                        <div class="row items-push">
                            <div class="col-lg-7 offset-lg-4">
                                <button type="button" class="btn btn-primary"  onclick="createRol()">Guarda</button>
                            </div>
                        </div>
                        <!-- END Submit -->
                    </div>
                </div>
            </div>
        </form>
        <!-- jQuery Validation -->
    </div>
    <!-- END Page Content -->
    </div>

</main>
<!-- END Main Container -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>

        function createRol() {
            var token = $('#token-auth').data('token');

            $.ajax({
                type: 'POST',
                data: $('#form-rol').serialize(),
                url: "<?php echo e(URL::to('/')); ?>/api/admin/roles/crear",
                headers: {
                    'Authorization': 'Bearer ' +token
                },
                success: function(data){
                    $("#alert-ok-div").hide();
                    $("#alert-ok").html(data.msj);
                    $("#alert-ok-div").show();

                },
                error: function(data){
                    $("#alert-error-div").hide();
                    $("#alert-error").html(data.msj);
                    $("#alert-error-div").show();
                    //Cuando la interacción retorne un error, se ejecutará esto.
                }

            });
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\proyectos\proyectos PHP\matedivertida1\resources\views/admin/new_rol.blade.php ENDPATH**/ ?>