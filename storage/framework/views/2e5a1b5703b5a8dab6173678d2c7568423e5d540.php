<?php $__env->startSection('content'); ?>

    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="<?php echo e(asset('/storage/3.png')); ?>" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <div class="mt-7 mb-5 text-center">
                        <h1 class="h2 text-secondary mb-2 invisible" data-toggle="appear" data-class="animated fadeInDown">#ColeEnCasa</h1>
                        <h2 class="h4 font-w400 text-secondary-75 invisible" data-toggle="appear" data-class="animated fadeInDown">Clases virtuales.</h2>
                        <a class="btn btn-rounded btn-primary px-4 py-2 invisible" data-toggle="appear" data-class="animated zoomIn" href="javascript:void(0)">Contacto 112222222</a>
                        <h2 class="h4 font-w400 text-secondary-75 invisible mt-2" data-toggle="appear" data-class="animated fadeInDown">Primaria - Secundaria</h2>

                    </div>
                </div>
            </div>












            <div class="carousel-item">
                <img src="<?php echo e(asset('/storage/4.png')); ?>" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <div class="mt-7 mb-5 text-center">
                        <h1 class="h2 text-secondary mb-2 invisible" data-toggle="appear" data-class="animated fadeInDown">#ColeEnCasa</h1>
                        <h2 class="h4 font-w400 text-secondary-75 invisible" data-toggle="appear" data-class="animated fadeInDown">Clases virtuales.</h2>
                        <a class="btn btn-rounded btn-primary px-4 py-2 invisible" data-toggle="appear" data-class="animated zoomIn" href="javascript:void(0)">Contacto 112222222</a>
                        <h2 class="h4 font-w400 text-secondary-75 invisible mt-2" data-toggle="appear" data-class="animated fadeInDown">Primaria - Secundaria</h2>

                    </div>
                </div>
            </div>

        </div>

        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


    <!-- END Page Content -->

    <!-- Instructors -->
    <div class="bg-image" style="background-image: url('<?php echo e(asset('/storage/fondo.png')); ?>');">
        <div class="bg-primary-dark-op py-5">
            <div class="content content-full content-boxed text-center">
                <h2 class="font-w400 text-white mb-2 invisible" data-toggle="appear" data-offset="50" data-class="animated fadeInDown">Aprende desde casa</h2>
                <h3 class="h4 font-w400 text-white-75 invisible" data-toggle="appear" data-timeout="300" data-class="animated fadeInDown">Puedes aprender y divertirte</h3>
                <div class="row items-push mt-5">
                    <!-- Page Content -->
                    <div class="content content-boxed">
                        <div class="row row-deck py-4 m-auto">
                            <!-- Course -->
                            <div class="col-md-6 col-lg-4 col-xl-3">
                                <a class="block block-rounded block-link-pop" href="#">
                                    <div class="block-content block-content-full text-center bg-warning" >
                                        <div class="item item-2x item-circle bg-white-10 py-3 my-3 mx-auto invisible" data-toggle="appear" data-offset="50" data-class="animated fadeIn">
                                            <i class="si si-user fa-2x text-white-75"></i>
                                        </div>
                                        <div class="font-size-sm text-white-75">
                                            <em></em> &bull;
                                        </div>
                                    </div>
                                    <div class="block-content block-content-full">
                                        <h5 class="mb-1">Tu cole en casa, aprendes de manera divertida.</h5>
                                        <div class="font-size-sm text-muted"></div>
                                    </div>
                                </a>
                            </div>
                            <!-- END Course -->

                            <!-- Course -->
                            <div class="col-md-6 col-lg-4 col-xl-3">
                                <a class="block block-rounded block-link-pop" href="#">
                                    <div class="block-content block-content-full text-center bg-flat" >
                                        <div class="item item-2x item-circle bg-white-10 py-3 my-3 mx-auto invisible" data-toggle="appear" data-offset="50" data-class="animated fadeIn">
                                            <i class="si si-camcorder fa-2x text-white-75"></i>
                                        </div>
                                        <div class="font-size-sm text-white-75">
                                            <em></em> &bull;
                                        </div>
                                    </div>
                                    <div class="block-content block-content-full">
                                        <h5 class="mb-1">Aula virtual</h5>
                                    </div>
                                </a>
                            </div>
                            <!-- END Course -->

                            <!-- Course -->
                            <div class="col-md-6 col-lg-4 col-xl-3">
                                <a class="block block-rounded block-link-pop" href="#">
                                    <div class="block-content block-content-full text-center bg-amethyst">
                                        <div class="item item-2x item-circle bg-white-10 py-3 my-3 mx-auto invisible" data-toggle="appear" data-offset="50" data-class="animated fadeIn">
                                            <i class="si si-rocket fa-2x text-white-75"></i>
                                        </div>
                                        <div class="font-size-sm text-white-75">
                                            <em></em> &bull;
                                        </div>
                                    </div>
                                    <div class="block-content block-content-full">
                                        <h5 class="mb-1">Variedad de cursos</h5>
                                        <div class="font-size-sm text-muted"></div>
                                    </div>
                                </a>
                            </div>
                            <!-- END Course -->

                            <!-- Course -->
                            <div class="col-md-6 col-lg-4 col-xl-3">
                                <a class="block block-rounded block-link-pop" href="#">
                                    <div class="block-content block-content-full text-center bg-city">
                                        <div class="item item-2x item-circle bg-white-10 py-3 my-3 mx-auto invisible" data-toggle="appear" data-offset="50" data-class="animated fadeIn">
                                            <i class="fab fa-edit fa-2x text-white-75"></i>
                                        </div>
                                        <div class="font-size-sm text-white-75">
                                            <em></em> &bull;
                                        </div>
                                    </div>
                                    <div class="block-content block-content-full">
                                        <h5 class="mb-1">Aprende desde casa.</h5>
                                        <div class="font-size-sm text-muted"></div>
                                    </div>
                                </a>
                            </div>
                            <!-- END Course -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- Get Started -->
    <div class="bg-body-dark">
        <div class="content content-full">
            <div class="my-5 text-center">
                <h3 class="h4 mb-4 invisible" data-toggle="appear">#aprendeDesdeCasa</h3>
                <a class="btn btn-rounded btn-primary px-4 py-2 invisible" data-toggle="appear" data-class="animated bounceIn" href="<?php echo e(url('/')); ?>/ingresar">Ingresar</a>
            </div>
        </div>
    </div>
    <!-- END Get Started -->

<!-- END Main Container -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('js/core/slider/slick.min.js')); ?>"></script>
    <script>jQuery(function(){ One.helpers('slick'); });</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.public', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\proyectos\proyectos PHP\Develop\matedivertida1\resources\views/inicio.blade.php ENDPATH**/ ?>