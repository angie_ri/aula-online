<?php $__env->startSection('content'); ?>

    <div class="content">

        <!-- Avatar Sliders -->
        <h2 class="content-heading">Materias</h2>
        <div class="row">
            <div class="col-md-12">
                <!-- Slider with Multiple Slides/Avatars -->
                <div class="block ">
                    <div class="block-header">
                        <h3 class="block-title">
                            Materias de clase
                        </h3>
                    </div>
                    <div class="block-content">
                        <div class="js-slider text-center" data-autoplay="true" data-dots="true" data-arrows="true" data-slides-to-show="5">

                        <div class="block invisible" data-toggle="appear" data-offset="-400">
                            <div class="block-content block-content-full">
                                <div class="py-5 text-center border ">
                                    <div class="item item-2x item-rounded bg-info-light text-info mx-auto ">
                                        <i class="si fa-2x si-notebook"></i>
                                    </div>
                                    <div class="font-size-h4 font-w600 pt-3 mb-0">Materias1</div>
                                </div>
                            </div>
                        </div>

                            <div class="block invisible" data-toggle="appear" data-offset="-400">
                                <div class="block-content block-content-full">
                                    <div class="py-5 text-center border ">
                                        <div class="item item-2x item-rounded bg-success-light text-success mx-auto ">
                                            <i class="si fa-2x si-notebook"></i>
                                        </div>
                                        <div class="font-size-h4 font-w600 pt-3 mb-0">Materias1</div>
                                    </div>
                                </div>
                            </div>
                            <div class="block invisible" data-toggle="appear" data-offset="-100">
                                <div class="block-content block-content-full">
                                    <div class="py-5 text-center border">
                                        <div class="item item-2x item-rounded bg-danger-light text-danger mx-auto">
                                            <i class="si fa-2x si-notebook"></i>
                                        </div>
                                        <div class="font-size-h4 font-w600 pt-3 mb-0">Materias1</div>
                                    </div>
                                </div>
                            </div>

                            <div class="block invisible" data-toggle="appear" data-offset="200">
                                <div class="block-content block-content-full">
                                    <div class="py-5 text-center border">
                                        <div class="item item-2x item-rounded bg-warning-light text-warning mx-auto">
                                            <i class="si fa-2x si-notebook"></i>
                                        </div>
                                        <div class="font-size-h4 font-w600 pt-3 mb-0">Materias1</div>
                                    </div>
                                </div>
                            </div>

                            <div class="block invisible" data-toggle="appear" data-offset="-400">
                                <div class="block-content block-content-full">
                                    <div class="py-5 text-center border">
                                        <div class="item item-2x item-rounded bg-success-light text-success mx-auto">
                                            <i class="si fa-2x si-notebook"></i>
                                        </div>
                                        <div class="font-size-h4 font-w600 pt-3 mb-0">Materias1</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- END Slider with Multiple Slides/Avatars -->
            </div>
        </div>
        <!-- END Avatar Sliders -->

        <!-- Timeouts -->
        <h2 class="content-heading">Opciones </h2>
        <div class="row">
            <div class="col-sm-4">
                <div class="block invisible" data-toggle="appear" data-offset="-200">
                    <div class="block-content block-content-full">
                        <div class="py-5 text-center">
                            <div class="item item-2x item-rounded bg-body text-primary mx-auto">
                                <i class="si fa-2x si-list"></i>
                            </div>
                            <div class="font-size-h4 font-w600 pt-3 mb-0">Materias</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="block invisible" data-toggle="appear" data-offset="-200" data-timeout="200">
                    <div class="block-content block-content-full">
                        <div class="py-5 text-center">
                            <div class="item item-2x item-rounded bg-body text-danger mx-auto">
                                <i class="si fa-2x si-notebook"></i>
                            </div>
                            <div class="font-size-h4 font-w600 pt-3 mb-0">Tareas</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="block invisible" data-toggle="appear" data-offset="-200" data-timeout="400">
                    <div class="block-content block-content-full">
                        <div class="py-5 text-center">
                            <div class="item item-2x item-rounded bg-body text-success mx-auto">
                                <i class="si fa-2x si-users"></i>
                            </div>
                            <div class="font-size-h4 font-w600 pt-3 mb-0">Profesores</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Timeouts -->

        <!-- Avatar Sliders -->
        <h2 class="content-heading">Alumnos</h2>
        <div class="row">
            <div class="col-md-12">
                <!-- Slider with Multiple Slides/Avatars -->
                <div class="block">
                    <div class="block-header">
                        <h3 class="block-title">
                            Compañero/as de clase
                        </h3>
                    </div>
                    <div class="block-content">
                        <div class="js-slider text-center" data-autoplay="true" data-dots="true" data-arrows="true" data-slides-to-show="3">
                            <div class="py-3">
                                <img class="img-avatar" src="" alt="">
                                <div class="mt-2 font-w600">Helen Jacobs</div>
                                <div class="font-size-sm text-muted">Graphic Designer</div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END Slider with Multiple Slides/Avatars -->
            </div>
        </div>
        <!-- END Avatar Sliders -->


    </div>
    <!-- END Page Content -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('js/core/slider/slick.min.js')); ?>"></script>
    <script>

        jQuery(function(){ One.helpers('slick'); });

    </script>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\proyectos\proyectos PHP\matedivertida1\resources\views/aula/clases_room.blade.php ENDPATH**/ ?>
