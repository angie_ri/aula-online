<?php $__env->startSection('content'); ?>
    <!-- Main Container -->
    <main id="main-container">
        <!-- Hero -->
        <div class="bg-body-light">
            <div class="content content-full">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                    <h1 class="flex-sm-fill h3 my-2">
                        Lista de Materias <small class="d-block d-sm-inline-block mt-2 mt-sm-0 font-size-base font-w400 text-muted">Las materias creadas, se puede editar y deshabilitar.</small>
                    </h1>
                    <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-alt">
                            <li class="breadcrumb-item">Ir a</li>
                            <li class="breadcrumb-item" aria-current="page">
                                <a class="link-fx" href="<?php echo e(url('/')); ?>/admin/materia/crear">Crear Materia</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Categories-->
        <div class="content">
            <!-- Partial Table -->
            <div class="block">
                <div class="block-header">
                    <h3 class="block-title">Tabla Materias</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option">
                            <i class="si si-settings"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="table-responsive">

                        <table class="table table-bordered table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th style="width: 30%;">Grado</th>
                            <th class="d-none d-sm-table-cell" style="width: 15%;">Estado</th>
                            <th class="d-none d-md-table-cell text-center" style="width: 100px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $materias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr id="mat-tabla-<?php echo e($mat->id); ?>">

                                <td class="font-w600 font-size-sm" id="name-tabla-<?php echo e($mat->id); ?>">
                                    <?php echo e($mat->nombre); ?>

                                </td>
                                <td class="d-sm-table-cell " id="grado-tabla-<?php echo e($mat->id); ?>"><?php echo e($mat->grado); ?></td>
                                <td class="d-md-table-cell font-size-sm" id="statu_tabla-<?php echo e($mat->id); ?>">
                                    <?php echo e($mat->statu == 1? 'Activo' :'Inactivo'); ?>

                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-block-slideright" title="Editar" data-materia="<?php echo e($mat); ?>" onclick="getMateria(this)">
                                            <i class="fa fa-fw fa-pencil-alt"></i>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Borrar"  data-idmat="<?php echo e($mat->id); ?>" onclick="deleteMateria(this)">
                                            <i class="fa fa-fw fa-times"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Partial Table -->
        </div>
        </div>
    </main>
    <!-- Slide Right Block Modal -->
    <div class="modal fade" id="modal-block-slideright" tabindex="-1" role="dialog" aria-labelledby="modal-block-slideright" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideright" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Editar Materia</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content font-size-sm">
                        <form class="js-validation" id="form-materia-edit" method="POST">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">Editar Materia</h3>
                                </div>
                                
                                <?php echo $__env->make('components.alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <div class="block-content block-content-full " id="block-modal">

                                    <?php echo csrf_field(); ?>
                                    <input id="id_mat" name="id" type="hidden">
                                    <div class="row items-push">
                                        <div class="col-lg-8 col-xl-10">
                                            <div class="form-group">
                                                <label for="val-username">Nombre </label>
                                                <input type="text" class="form-control" id="name_mat" name="nombre" placeholder="Nombre">
                                            </div>
                                            <div class="form-group">
                                                <label for="val-username">Grado</label>
                                                <input type="number" class="form-control" id="grado_mat" name="grado" placeholder="Grado">
                                            </div>

                                            <div class="form-group">
                                                <label for="val-skill">Estado</label>
                                                <select class="form-control" id="statu_mat" name="statu">
                                                    <option value="">Seleccione</option>
                                                    <option value="1">Activo</option>
                                                    <option value="2">Inactivo</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- END Regular -->

                                    <!-- Submit -->
                                    <div class="row items-push">
                                        <div class="col-lg-7 offset-lg-4">
                                            <button type="button" class="btn btn-primary" onclick="editMateria()">Guarda</button>
                                        </div>
                                    </div>
                                    <!-- END Submit -->
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="block-content block-content-full text-right border-top">
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Slide Right Block Modal -->


<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>

        /**
         *
         * */
        function getMateria(materia) {
            var materia =$(materia).data('materia');

            $("#name_mat").val(materia.nombre);
            $("#grado_mat").val(materia.grado);
            $("#id_mat").val(materia.id);
        }

        /**
         * Editar Rol
         **/
        function editMateria() {

            var token = $('#token-auth').data('token');

            $.ajax({
                type: 'POST',
                data: $('#form-materia-edit').serialize(),
                url: "<?php echo e(URL::to('/')); ?>/api/admin/materia/editar",

                headers: {
                    'Authorization': 'Bearer ' +token
                },
                success: function(data){

                    $("#alert-ok2").html(data.msj);
                    $("#alert-ok2-div").show();
                    var id = data.materia.id;
                    $("#grado-tabla-"+id).html(data.materia.grado);
                    $("#name-tabla-"+id).html(data.materia.nombre);

                    if(data.materia.statu == 1){
                        $("#statu_tabla-"+id).html("Activo");
                    }else{
                        $("#statu_tabla-"+id).html("Inactivo");
                    }
                    setTimeout(function() {
                        $("#alert-ok2-div").fadeOut();
                    },5000);

                },
                error: function(data){

                    $("#alert-ok2").html(data.msj);
                    $("#alert-ok2-div").show();

                    setTimeout(function() {
                        $("#alert-ok2-div").fadeOut();
                    },4000);

                }

            });
        }

        /**
         * elimina el rol
         * @param  id
         */
        function deleteMateria(id) {

            var id_mat = $(id).data('idmat');
            var token = $('#token-auth').data('token');
            var name_mat =$("#name-tabla-"+id_mat).text();

            Swal.fire({
                title: 'Quiere eliminar la materia?',
                text: "Materia "+name_mat+" será eliminada!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#fb3636',
                confirmButtonText: 'Si, eliminar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {

                    $.ajax({
                        type: 'GET',
                        url: "<?php echo e(URL::to('/')); ?>/api/admin/materia/eliminar/"+id_mat,
                        headers: {
                            'Authorization': 'Bearer ' +token
                        },
                        success: function(data){
                            $("#mat-tabla-"+id_mat).hide();

                            Swal.fire(
                                'Eliminado!',
                                data.msj,
                            );

                        },
                        error: function(data){

                            Swal.fire(
                                'Error!',
                                data.responseJSON.msj,
                            );

                        }

                    });
                }
            });



        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\proyectos\proyectos PHP\Develop\matedivertida1\resources\views/admin/materias.blade.php ENDPATH**/ ?>