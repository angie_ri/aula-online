
    <!-- Hero Content -->
    <?php if(auth()->guard()->check()): ?>
    <div class="bg-image" style="background-image: url('<?php echo e(asset('/imagen/fondo3.png')); ?>')"  >
        <div class="bg-black-50">
            <div class="content content-full overflow-hidden">
                <div class="mt-7 mb-5 text-center">
                    <h1 class="h2 text-white mb-2 invisible" data-toggle="appear" data-class="animated fadeInDown">MATEDIVERTIDA con Teacher Juan</h1>
                    <h2 class="h4 font-w400 text-white-75 invisible" data-toggle="appear" data-class="animated fadeInDown">Clases virtuales de Matemáticas.</h2>
                    <a class="btn btn-rounded btn-success px-4 py-2 invisible" data-toggle="appear" data-class="animated zoomIn" href="javascript:void(0)">Contacto +51 978401020</a>
                    <h2 class="h4 font-w400 text-white-75 invisible mt-2" data-toggle="appear" data-class="animated fadeInDown">Primaria - Secundaria</h2>

                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
<?php /**PATH E:\proyectos\proyectos PHP\matedivertida1\resources\views/clases_room.blade.php ENDPATH**/ ?>
