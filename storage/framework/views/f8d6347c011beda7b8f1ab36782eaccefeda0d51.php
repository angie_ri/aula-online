<?php $__env->startSection('content'); ?>
<!-- Page Content -->
<div class="bg-image ">
    <div class="hero-static "style="background-image: url('<?php echo e(asset('imagen/fondo.png')); ?>');" >
        <div class="content ">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-4">
                    <!-- Sign In Block -->
                    <div class="block block-themed  mb-0 ">
                        <div class="block-header">
                            <h3 class="block-title">Ingresar</h3>
                            <div class="block-options">
                                <a class="btn-block-option" href="<?php echo e(url('/')); ?>/registrar" data-toggle="tooltip" data-placement="left" title="Nueva cuenta">Registrarse
                                    <i class="fa fa-user-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="block-content" >
                            <div class="p-sm-3 px-lg-4 py-lg-5">
                                <h1 class="mb-2">MateDivertida</h1>
                                <p>Bienvenido, por favor ingrese.</p>

                                <form class="js-validation-signin" id="form-ingresar" method="post" >
                                    <div class="py-3">

                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-alt form-control-lg" id="login-username" name="email" placeholder="Correo electrónico">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-alt form-control-lg" id="login-password" name="password" placeholder="Contraseña">
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6 col-xl-5">
                                            <button  class="btn btn-block btn-primary" type="button" id="ingresar" onclick="login();" >
                                                <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Ingresar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- END Sign In Form -->
                            </div>
                        </div>
                    </div>
                    <!-- END Sign In Block -->
                </div>
            </div>
        </div>
        <div class="content content-full font-size-sm text-muted text-center">
            <strong>OneUI 4.3</strong> &copy; <span data-toggle="year-copy"></span>
        </div>
    </div>
</div>
<!-- END Page Content -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('js/jquery/jquery-3.5.0.js')); ?>"></script>
    <script>
        $( document ).ready(function() {

            // ingresar();
            // $('#ingresar').on('click',function () {
            //     console.log('hshas');
            // })
        });

        function login() {

            $.ajax({
                data: $('#form-ingresar').serialize(),
                url: "<?php echo e(URL::to('/')); ?>/api/ingresar",
                type: "POST",
                success: function (data) {

                    console.log(data);

                },
                error: function (data) {

                }
            });
        }
    </script>
    <?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.public', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\proyectos\PHP\matedivertida\resources\views/users/ingresar.blade.php ENDPATH**/ ?>