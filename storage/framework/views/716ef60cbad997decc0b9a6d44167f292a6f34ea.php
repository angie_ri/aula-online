<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <title> ColeEnCasa <?php echo $__env->yieldContent('title'); ?></title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
        <link rel="stylesheet" id="css-main" href=" <?php echo e(asset('css/oneui.min.css')); ?>">
        <link rel="stylesheet" id="css-main" href=" <?php echo e(asset('css/bootstrap.min.css')); ?>">
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="<?php echo e(asset('css/slider/slick.css')); ?>">

        <link rel="stylesheet" href="<?php echo e(asset('css/slider/slick-theme.css')); ?>">

    </head>
    <body>

    <?php $__env->startSection('sidebar'); ?>

        <?php echo $__env->yieldSection(); ?>
            <!-- Main Container -->
            <main id="main-container">
                <?php echo $__env->yieldContent('content'); ?>
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="bg-body-light ">
                <div class="content py-3">
                    <div class="row font-size-sm">
                        <div class="col-sm-6 order-sm-1 py-1 ">
                            <a class="font-w400 text-info " href="#" target="_blank">Desarrollo web <strong>ArSoft</strong> </a> &copy; 2020</span>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->



        <!-- END Page Container -->

        <script src="<?php echo e(asset('/js/oneui.core.min.js')); ?>"></script>
        <script src="<?php echo e(asset('/js/oneui.app.min.js')); ?>"></script>


    <?php echo $__env->yieldContent('scripts'); ?>
    </body>
</html>
<?php /**PATH E:\proyectos\proyectos PHP\Develop\matedivertida1\resources\views/layouts/public.blade.php ENDPATH**/ ?>