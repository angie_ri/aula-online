<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table ="role_users";

    protected $fillable = [
        'id_user', 'id_role',
    ];

    public function role()
    {
        return $this->hasOne('App\Model\Role','id','id_role');

    }
}
