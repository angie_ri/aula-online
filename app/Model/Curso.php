<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table ="cursos";

    protected $fillable = [
        'id', 'nombre', 'grado','statu',
    ];
    public function profeMateria()
    {
        return $this->hasMany('App\Model\ProfesorMateria','id_materia','id');
    }
}
