<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    protected $table ="profesors";

    protected $fillable = [
        'id_user', 'id_materia','apodo','statu'
    ];

    public  function  user()
    {
        //foreing=id de User y locakey =id_user (clave foranea)
        return $this->hasOne('App\User','id','id_user');
    }

    public  function materias()
    {
        return $this->hasMany('App\Model\ProfesorMateria','id_profesor','id');
    }

    //a probar
    public function  materiaProfesor()
    {

        $this->belongsToMany('App\Model\Curso', 'profesor_materias','id_profesor', 'id_materia');

    }
}
