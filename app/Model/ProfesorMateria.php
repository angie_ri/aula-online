<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProfesorMateria extends Model
{
    protected $table ="profesor_materias";

    protected $fillable = [
        'id_profesor', 'id_materia','id'
    ];

    public function materia()
    {
        return $this->hasOne('App\Model\Curso','id','id_materia');
    }



}
