<?php

namespace App\Http\Controllers;

use App\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;


class AuthController extends Controller
{

    protected function generateAccessToken($user)
    {
        $token = $token = $user->createToken('matedivertida')->accessToken;
//        $user->createToken($user->email.'-'.now());

        return $token;
    }

    public function register(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => hash::make($request->password),
            'statu' => 1,
        ]);
        return response()->json(200);

    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required',

        ]);
        $password = Hash::check('plain-text', $request->password);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();

            $token = $user->createToken('matedivertida1')->accessToken;
            $user->remember_token = $token;
            $user->save();

            $role = Role::where('slug','admin')->first(['id']);

            if($user->rolUser && $user->rolUser->id_role == $role->id)
            {
                return redirect()->action('Admin\AdminController@index');
            }else{
                return redirect()->action('HomeController@show');
            }
        }else{

            return view('users.ingresar');
        }

    }


    public function  logout(){

        $user = Auth::user();
        $user->tokens()->delete();
        $user->remember_token ='';
        $user->save();
        Auth::logout();
        return redirect('/');

    }

}
