<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('inicio');


        $this->middleware('auth:api')->only('index');
        //EJM
//        $this->middleware('auth:api')->only('index');
//        $this->middleware('subscribed')->except('store');

    }

    public function inicio(Request $request)
    {
        return view('inicio');
    }
    public function index(Request $request)
    {
        return view('home');
    }

    public function show()
    {
        $auth = Auth::user();
        return view('aula.clases_room',compact('auth'));
    }
}
