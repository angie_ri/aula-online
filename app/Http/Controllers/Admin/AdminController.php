<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Curso;
use App\Model\Profesor;
use App\Model\profesorMateria;
use App\Model\Role;
use App\Model\RoleUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
//        $this->middleware('auth:api')->only(['editarUsuario','newRol','editRole']);
    }

    public  function  index()
    {
        $auth = Auth::user();

        return view('admin.index',compact('auth'));
    }

    public  function  roles()
    {
        $auth = Auth::user();
        $roles = $this->getRole();
        return view('admin.roles',compact('auth','roles'));
    }

    public  function  crearRol()
    {
        $auth = Auth::user();
        return view('admin.new_rol',compact('auth'));
    }

    public function  deleteRole($id_rol)
    {
        $rol = Role::find($id_rol);
        $rol->statu = 2;
        $rol->save();

        if($rol->statu == 2){
            return response()->json(['msj'=>'Rol Inactivo'],200);

        }else{
            return response()->json(['msj'=>'Error al cambiar estado de Rol'],404);

        }
    }
    public function  newRol(Request $request){
        try{
            DB::beginTransaction();
        $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'statu' => 'required',
        ]);

        $rol = Role::create([
            'name'=> $request->name,
            'slug'=>$request->slug,
            'statu'=>$request->statu,
        ]);

            DB::commit();
            return response()->json(['msj'=>'Nuevo Rol creado!',],200);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['msj'=>"Error al crear Rol.",$e],404);
        }
    }

    /**
     * editar role
     */
    public function editRole(Request $request)
    {

        try{

            $role = Role::find($request->id);

            if($role->name != $request->name)
            {
                $role->name = $request->name;

            }
            if($role->slug != $request->slug)
            {
                $role->slug = $request->slug;
            }

            if($request->statu != null)
            {
                $role->statu = $request->statu;
            }
                $role->save();

            return response()->json(['msj'=>"Rol actualizado.", "rol"=>$role],200);

        }
        catch (\Exception $e){

            return response()->json(['msj'=>"El nombre y slug debe ser diferente"],422);

        }

    }


    //usuarios
    public  function  usuarios()
    {
        $auth= Auth::user();
        $users = User::all(['id','name','email','statu'])->where('statu','<>',3);
        $roles = $this->getRole();

        return view('admin.users',compact('auth','users','roles'));
    }

    public function  editarUsuario(Request $request){

        try{
            DB::beginTransaction();
            $user = User::find($request->id);

            if($request->statu != null){
                $user->statu = $request->statu;
                $user->save();
            }

            if($request->rol != null)
            {

                $userRol =RoleUser::where('id_user',$request->id)->first();

                if(!$userRol)
                {
                    RoleUser::create([
                        'id_user' => $request->id,
                        'id_role' =>$request->rol,
                    ]);
                }else{
                    $userRol->id_role = $request->rol;
                    $userRol->save();
                }
            }

            DB::commit();
            return response()->json(['msj'=>'Actualización exitosa!','rol'=>$user->rolUser->role->name,'statu'=>$user->statu],200);

        }catch (\Exception $e){
                DB::rollback();
            return response()->json(['msj'=>"Error al actualizar usuario."],404);
        }
    }


    /**
     * si estado es 3 es bloqueado o sin permisos
     * @param $id_user
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUsuario($id_user)
    {
        $user = User::find($id_user);
        $user->statu = 3;
        $user->save();

        if($user->statu == 3) {
            return response()->json(['msj' => 'Usuario bloqueado'], 200);
        }else{
            return response()->json(['msj' => 'Error al bloquear usuario'], 404);

        }
    }

    //materias

    public  function  materias()
    {
        $auth = Auth::user();
        $materias =Curso::all();

        return view('admin.materias',compact('auth','materias'));
    }

    public  function  getMateria()
    {
        $auth = Auth::user();


        return view('admin.new_materia',compact('auth'));
    }

    public function newMateria(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'nombre' => 'required',
                'grado' => 'required',
                'statu' => 'required',
            ]);

            $materia = Curso::create([
                'nombre' => $request->nombre,
                'grado' => $request->grado,
                'statu' => $request->statu,
            ]);

            DB::commit();

            return response()->json(['msj' => 'Nueva materia creada!',], 200);
        }
        catch (\Exception $e)
        {
            return response()->json(['msj' => 'Error al crer materia!',$e],402);

        }
    }

    public function editMateria(Request $request )
    {
        try {

            $materia = Curso::find($request->id);

            if ($request->nombre) {
                $materia->nombre = $request->nombre;
            }

            if ($request->grado) {
                $materia->grado = $request->grado;
            }

            if ($request->statu) {
                $materia->statu = $request->statu;
            }
            $materia->save();

            return response()->json(['msj' => 'Materia editada!','materia'=>$materia], 200);
        }
        catch (\Exception $e)
        {
            return response()->json(['msj' => 'Error al editar materia!',$e],402);

        }
    }


    public function deleteMateria($id_materia)
    {
        try{
        $materia = Curso::find($id_materia);
        $materia->statu = 3;
        $materia->save();


        return response()->json(['msj' => 'Materia eliminada!'], 200);
    }
        catch (\Exception $e)
        {
        return response()->json(['msj' => 'Error al eliminar materia!',$e],402);

        }
    }

    //profesores

    public  function  profesor()
    {
        $auth = Auth::user();
        $materias =Curso::all();
        $profesores = Profesor::all();

        return view('admin.profesores',compact('auth','materias','profesores'));
    }

    public  function  crearProfesor()
    {
        $auth = Auth::user();
        $materias =Curso::all();
        $profesores = $this->getProfesores();

        return view('admin.new_profesor',compact('auth','materias','profesores'));
    }

    public function newProfesor(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'id_user' => 'required',
                'apodo' => 'required',
                'statu' => 'required',
                'id_materia' =>'required'
            ]);

            $profesor = Profesor::create([
                'id_user' => $request->id_user,
                'apodo' => $request->apodo,
                'statu' => $request->statu,
            ]);

            ProfesorMateria::create([
                'id_profesor'=>$profesor->id,
                'id_materia'=>$request->id_materia,
            ]);

            DB::commit();

            return response()->json(['msj' => 'Nuevo profesor creado!',], 200);
        }
        catch (\Exception $e)
        {
            return response()->json(['msj' => 'Error al crer profesor!',$e],402);

        }
    }


    public function getMaterias($id_profe)
    {

        if($id_profe)
        {
            $profe = Profesor::find($id_profe);
            $claves =[];
            $materias=$profe->materias;

            foreach ($materias as $mat)
            {
                $claves[]= $mat->id;
            }
            $materias= Curso::whereIn('id',$claves)->get();
            $select= Curso::whereNotIn('id',$claves)->get();


            return response()->json(['materias' =>$materias,'select'=>$select]);

        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function asignarMateria(Request $request)
    {
        $materia = ProfesorMateria::create([
            'id_profesor'=> $request->id_profesor,
            'id_materia'=> $request->id_materia,
        ]);

        $profe = Profesor::find($request->id_profesor);
        $claves =[];
        $materias=$profe->materias;
        foreach ($materias as $mat)
        {
            $claves[]= $mat->id;
        }

        $materias= Curso::whereIn('id',$claves)->get();
        $select= Curso::whereNotIn('id',$claves)->get();

        return response()->json(['materias' =>$materias,'select'=>$select]);
    }


    public function editProfesor(Request $request)
    {
        $profesor = Profesor::find($request->id);

        if($request->apodo)
        {
            $profesor->apodo = $request->apodo;
        }

        if($request->statu)
        {
            $profesor->statu = $request->statu;
        }
        $profesor->save();

        return response()->json(['msj' => 'Profesor editado!',"profesor"=>$profesor], 200);

    }


    public function deleteProfesor($id_profe)
    {
        $profesor = Profesor::find($id_profe);
        $profesor->statu = 3;
        $profesor->save();

        if($profesor->statu == 3)
        {
            return response()->json(['msj' => 'Profesor eliminado!'], 200);
        }else{
            return response()->json(['msj' => 'Error al eliminar profesor!'], 402);

        }
    }


    private function  getRole()
    {
        return Role::all();
    }

    private function  getProfesores()
    {
        $profesores = User::whereHas('rolUser', function (Builder $query) {
            $query->where('id_role', '2');
        })->whereIn('statu', [1,2])->get();

        return $profesores;
    }
}
