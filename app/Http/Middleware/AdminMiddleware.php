<?php

namespace App\Http\Middleware;

use App\Model\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Role::where('slug','admin')->first(['id']);
        if (auth()->check() && auth()->user()->rolUser->id_role == $role->id || $request->bearerToken())
        {

            return $next($request);
        }
    }
}
