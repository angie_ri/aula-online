<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@inicio');
//Route::get('/', function () {
//    return view('inicio');
//});
Route::get('/registrar', function () {
    return view('users/registrar');
});
Route::get('/ingresar', function () {
    return view('users/ingresar');
});

Route::post('/ingresar','AuthController@login');

Route::middleware('auth')->group(function() {

    Route::get('/logout', 'AuthController@logout');
    Route::get('/user/home', 'HomeController@index');
    Route::get('/clases/sala', 'HomeController@show');

});

Route::middleware('admin')->group(function() {

    Route::get('/admin', 'Admin\AdminController@index');
    Route::get('/admin/roles/lista', 'Admin\AdminController@roles');
    Route::get('/admin/roles/crear', 'Admin\AdminController@crearRol');
    Route::get('/admin/usuario/lista', 'Admin\AdminController@usuarios');
    Route::get('/admin/usuario/crear', 'Admin\AdminController@crearUsuario');
    Route::get('/admin/materia/lista', 'Admin\AdminController@materias');
    Route::get('/admin/materia/crear', 'Admin\AdminController@getMateria');
    Route::get('/admin/profesor/lista', 'Admin\AdminController@profesor');
    Route::get('/admin/profesor/crear', 'Admin\AdminController@crearProfesor');


});
