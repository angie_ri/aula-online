<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('/registrar', 'AuthController@register');
//Route::post('/ingresar', 'AuthController@login');

Route::middleware('auth:api')->group(function() {

    Route::get('/user/home', 'HomeController@index');
//    //admin user
//    Route::post('/admin/usuario/editar', 'Admin\AdminController@editarUsuario');
//    Route::get('/admin/usuario/eliminar/{id_user}', 'Admin\AdminController@deleteUsuario');
//
//    //role admin
//    Route::post('/admin/roles/crear', 'Admin\AdminController@newRol');
//    Route::get('/admin/role/eliminar/{id_rol}', 'Admin\AdminController@deleteRole');
//    Route::post('admin/role/editar','Admin\AdminController@editRole');

});

Route::group(['middleware' => ['auth:api', 'admin']], function () {
    // your protected routes.
    Route::post('/admin/usuario/editar', 'Admin\AdminController@editarUsuario');

});

Route::middleware(['admin:api','admin'])->group(function() {

    //admin user
    Route::post('/admin/usuario/editar', 'Admin\AdminController@editarUsuario');
    Route::get('/admin/usuario/eliminar/{id_user}', 'Admin\AdminController@deleteUsuario');

    //role admin
    Route::post('/admin/roles/crear', 'Admin\AdminController@newRol');
    Route::get('/admin/role/eliminar/{id_rol}', 'Admin\AdminController@deleteRole');
    Route::post('/admin/role/editar','Admin\AdminController@editRole');

    //materias
    Route::post('/admin/materia/crear','Admin\AdminController@newMateria');
    Route::post('/admin/materia/editar','Admin\AdminController@editMateria');
    Route::get('/admin/materia/eliminar/{id_materia}','Admin\AdminController@deleteMateria');

    //profesores
    Route::post('/admin/profesor/crear','Admin\AdminController@newProfesor');
    Route::get('/admin/profe/materias/{id_profe}','Admin\AdminController@getMaterias');
    Route::post('/admin/asignar/materia/','Admin\AdminController@asignarMateria');
    Route::post('/admin/profesor/editar','Admin\AdminController@editProfesor');
    Route::get('/admin/profesor/eliminar/{id_profe}','Admin\AdminController@deleteProfesor');


});
